<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Payment
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @BelongsTo(target="App\Models\Student") */
    private $student;

    /** @Column(type="string") */
    private $amount;

    /** @Column(type="string") */
    private $date;

    /** @Column(type="string") */
    private $method;

    /** @Column(type="string") */
    private $subject;

    /** @Column(type="string") */
    private $subjectIdentifier;

    /** @Column(type="string") */
    private $note;

    public function __construct() {

    }

    public function getStudent() {
        return $this->student;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getDate() {
        return $this->date;
    }

    public function getMethod() {
        return $this->method;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getSubjectIdentifier() {
        return $this->subjectIdentifier;
    }

    public function getNote() {
        return $this->note;
    }

    public function setStudent($value) {
        $this->student = $value;
    }

    public function setAmount($value) {
        $this->amount = $value;
    }

    public function setDate($value) {
        $this->date = $value;
    }

    public function setMethod($value) {
        $this->method = $value;
    }

    public function setSubject($value) {
        $this->subject = $value;
    }

    public function setSubjectIdentifier($value) {
        $this->subjectIdentifier = $value;
    }

    public function setNote($value) {
        $this->note = $value;
    }
}
