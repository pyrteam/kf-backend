<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Expense
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $category;

    /** @Column(type="string") */
    private $amount;

    /** @Column(type="string") */
    private $date;

    /** @Column(type="string") */
    private $note;

    public function __construct() {

    }

    public function getName() {
        return $this->name;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getDate() {
        return $this->date;
    }

    public function getNote() {
        return $this->note;
    }

    public function setName($value) {
        $this->name = $value;
    }

    public function setCategory($value) {
        $this->category = $value;
    }

    public function setAmount($value) {
        $this->amount = $value;
    }

    public function setDate($value) {
        $this->date = $value;
    }

    public function setNote($value) {
        $this->note = $value;
    }
}
