<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Inventory
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $price;

    /** @Column(type="boolean") */
    private $isForSale;

    /** @Column(type="integer") */
    private $quantity;

    /** @Column(type="string") */
    private $category;

    public function __construct() {

    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getIsForSale() {
        return $this->isForSale;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setName($value) {
        $this->name = $value;
    }

    public function setPrice($value) {
        $this->price = $value;
    }

    public function setIsForSale($value) {
        $this->isForSale = $value;
    }

    public function setQuantity($value) {
        $this->quantity = $value;
    }

    public function setCategory($value) {
        $this->category = $value;
    }
}
