<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Activity
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $rate;

    public function __construct() {

    }

    public function getName() {
        return $this->name;
    }

    public function getRate() {
        return $this->rate;
    }

    public function setName($value) {
        $this->name = $value;
    }

    public function setRate($value) {
        $this->rate = $value;
    }
}
