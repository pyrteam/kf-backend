<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class StudentActivity
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @BelongsTo(target="App\Models\Student") */
    private $student;

    /** @BelongsTo(target="App\Models\Activity") */
    private $activity;

    /** @BelongsTo(target="App\Models\Subscription") */
    private $subscription;

    /** @Column(type="string") */
    private $startDate;

    public function __construct() {

    }

    public function getStudent() {
        return $this->student;
    }

    public function getActivity() {
        return $this->activity;
    }

    public function getSubscription() {
        return $this->subscription;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStudent($value) {
        $this->student = $value;
    }

    public function setActivity($value) {
        $this->activity = $value;
    }

    public function setSubscription($value) {
        $this->subscription = $value;
    }

    public function setStartDate($value) {
        $this->startDate = $value;
    }
}
