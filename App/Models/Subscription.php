<?php

namespace App\Models;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Subscription
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @BelongsTo(target="App\Models\Activity") */
    private $activity;

    /** @Column(type="string") */
    private $rate;

    /** @Column(type="string") */
    private $period;

    public function __construct() {

    }

    public function getActivity() {
        return $this->activity;
    }

    public function getRate() {
        return $this->rate;
    }

    public function getPeriod() {
        return $this->period;
    }

    public function setActivity($value) {
        $this->activity = $value;
    }

    public function setRate($value) {
        $this->rate = $value;
    }

    public function setPeriod($value) {
        $this->period = $value;
    }
}
