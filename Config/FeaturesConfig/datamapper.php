<?php

return [

    'mappingDriver' => 'annotation',

    'databaseDriver' => 'mysql',

    'mysql' => [
        'host' => getenv('DATABASE_HOST'),
        'database' => getenv('DATABASE_NAME'),
        'username' => getenv('DATABASE_USERNAME'),
        'password' => getenv('DATABASE_PASSWORD')
    ],

    'classes' => [

        App\Models\Activity::class,
        App\Models\Student::class,
        App\Models\Subscription::class,
        App\Models\Payment::class,
        App\Models\StudentActivity::class,
        App\Models\Expense::class,
        App\Models\Inventory::class,
    ]

];