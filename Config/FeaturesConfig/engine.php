<?php

return [

    /**
     * The uri for engine requests.
     * The http verb is always POST.
     */
    'uri' => '/api/engine',

    /**
     * Path to model classes.
     */
    'modelsPath' => 'App/Models',

    /**
     * Path to engine controller classes.
     */
    'controllersPath' => 'App/Controllers'

];