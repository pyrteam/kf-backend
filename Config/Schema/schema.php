<?php

return [
    'activity' => [
        'name' => ['type' => 'string'],
        'rate' => ['type' => 'string']
    ],
    'student' => [
        'firstName' => ['type' => 'string'],
        'lastName' => ['type' => 'string']
    ],
    'subscription' => [
        'activity' => ['belongsTo' => 'activity'],
        'rate' => ['type' => 'string'],
        'period' => ['type' => 'string']
    ],
    'studentActivity' => [
        'student' => ['belongsTo' => 'student'],
        'activity' => ['belongsTo' => 'activity'],
        'subscription' => ['belongsTo' => 'subscription'],
        'startDate' => ['type' => 'string']
    ],
    'payment' => [
        'student' => ['belongsTo' => 'student'],
        'amount' => ['type' => 'string'],
        'date' => ['type' => 'string'],
        'method' => ['type' => 'string'],
        'subject' => ['type' => 'string'],
        'subjectIdentifier' => ['type' => 'string'],
        'note' => ['type' => 'string']
    ],
    'inventory' => [
        'name' => ['type' => 'string'],
        'price' => ['type' => 'string'],
        'isForSale' => ['type' => 'boolean'],
        'quantity' => ['type' => 'integer'],
        'category' => ['type' => 'string']
    ],
    'expense' => [
        'name' => ['type' => 'string'],
        'category' => ['type' => 'string'],
        'amount' => ['type' => 'string'],
        'date' => ['type' => 'string'],
        'note' => ['type' => 'string']
    ]
];